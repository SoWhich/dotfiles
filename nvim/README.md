# Neovim config

Make sure to clone the submodules, otherwise things will be broken.

This config expects latest versioned neovim (i. e. not nightly), which at time
of writing is 0.6.1, but I might forget to update this line as neovim updates.

There's some very WIP stuff in here, especially related to nicer package
management, but there's good stuff too.

Probably the statusline is the most interesting part (`lua/statusline.lua`),
but if you're looking for other interesting stuff I have my wip colorschime
(`colors/mycolors.vim`) and there are some utility functions in the main
`init.lua` that might be interesting.
