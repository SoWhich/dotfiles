local pkg = require 'pkg'
local keys = require 'keys'
local speak = require 'speak'
local util = require 'util'

vim.g.speakargs = {'-s', '300'}

vim.opt.number = true

vim.opt.list = true
vim.opt.listchars = util.join { 'tab: ⣀', 'trail:░' }

vim.opt.undofile = true

vim.opt.completeopt = util.join {'menuone', 'noselect'}

if vim.fn.executable('rg') then
  vim.opt.grepprg = 'rg --vimgrep $*'
end

vim.opt.wildmode = 'longest:full'

vim.opt.inccommand = 'nosplit'

vim.opt.termguicolors = true
vim.opt.pumblend = 5

vim.opt.guicursor = ""

vim.cmd 'colorscheme mycolors'

local default_opts = {silent = true};
local plug_opts = {silent = true, remap = true};

vim.g.mapleader = ' ';

vim.g.tex_flavor = 'latex'

function termrun()
  vim.ui.input({
      prompt = '> ',
      default = '',
      completion = 'shellcmd'
    },
    function (cmd) vim.cmd(":tab terminal " .. cmd); end
  );
end

keys.multimap {
  { 't', '<Esc><Esc>', '<C-\\><C-n>', default_opts },
  { '', '<Leader>t', termrun, default_opts },
  { '', '<Leader>e', vim.diagnostic.open_float, default_opts },
}

keys.multimap {
  { '', '<Leader>r', speak.operator, {expr=true}},
  { '', '<Leader>r.', speak.cancel, {expr=true, nowait=true}},
  { '', '<Leader>rr', speak.line, {}},
  { '', '<Leader>R', '<Leader>r$', {remap=true, nowait=true}},
  { '', '<Leader>rp', '<Leader>rap', {remap=true, nowait=true}},
}


keys.multibrace {
  { 'q', '<cmd>cnext<CR>', '<cmd>cprevious<CR>', default_opts },
  { 'e', vim.diagnostic.goto_next, vim.diagnostic.goto_prev, default_opts },
  { 't', '<cmd>tabnext<CR>', '<cmd>tabprevious<CR>', default_opts },
}

vim.cmd "autocmd BufEnter * lua (require'colorcolumn')()"
vim.cmd "normal helptags ALL"


pkg 'plenary.nvim'

pkg 'surround'
pkg 'vim-repeat'
pkg 'vim-sleuth'

keys.multimap {
  {'',  's',  '<Plug>ReplaceWithRegisterOperator',  plug_opts},
  {'',  'S',  's$', plug_opts},
  {'',  'ss', '<Plug>ReplaceWithRegisterLine',      plug_opts},
  {'x', 's',  '<Plug>ReplaceWithRegisterVisual',    plug_opts}
}
pkg 'ReplaceWithRegister'


pkg 'vim-fsharp'

keys.multimap {
  {'is', '<C-j>', function() vim.snippet.jump(1)  end, plug_opts},
  {'is', '<C-k>', function() vim.snippet.jump(-1) end, plug_opts}
}
pkg 'friendly-snippets'

vim.g["conjure#mapping#enable_defaults"] = false
vim.g["conjure#mapping#eval_root_form"] = {'-<BS>'}
vim.g["conjure#mapping#eval_visual"] = {'-'}
vim.g["conjure#mapping#eval_motion"] = {'-'}
vim.g["conjure#mapping#eval_current_form"] = {'- '}
vim.g["conjure#mapping#eval_buf"] = {'_'}

pkg 'conjure'

keys.multimap {
  {'', '<C-->v',     '<Cmd>ConjureLogVSplit<CR>',       plug_opts},
  {'', '<C--><C-v>', '<Cmd>ConjureLogVSplit<CR>',       plug_opts},
  {'', '<C-->s',     '<Cmd>ConjureLogSplit<CR>',        plug_opts},
  {'', '<C--><C-s>', '<Cmd>ConjureLogSplit<CR>',        plug_opts},
  {'', '<C-->b',     '<Cmd>ConjureLogBuf<CR>',          plug_opts},
  {'', '<C--><C-b>', '<Cmd>ConjureLogBuf<CR>',          plug_opts},
  {'', '<C-->-',     '<Cmd>ConjureLogToggle<CR>',       plug_opts},
  {'', '<C--><C-->', '<Cmd>ConjureLogToggle<CR>',       plug_opts},
  {'', '<C--><C-o>', '<Cmd>ConjureLogJumpToLatest<CR>', plug_opts}
}

pkg {'nvim-autopairs', setup = {check_ts = true}}

pkg 'cmp-nvim-lsp'
pkg {'nvim-cmp',
  require = 'cmp',
  fn = function(cmp)
    cmp.setup({
      sources = { { name = 'nvim_lsp' } },
      preselect = 'none',
      mapping = cmp.mapping.preset.insert()
    })
    cmp.event:on(
      'confirm_done',
      require 'nvim-autopairs.completion.cmp'.on_confirm_done({map_char = {tex = ''}})
    )
  end
}

pkg {'neogit', setup = {}}
keys.map {'', '<leader>g', function ()  require('neogit').open({kind="floating"}); end , plug_opts};

pkg {'telescope.nvim',
  require = 'telescope',
  setup = {
    defaults = {
      winblend = 25,
      prompt_prefix = '🔍︎ ',
      selection_caret = '▸',
      multi_icon = '» ',
      -- border = false,
      borderchars = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
      set_env = { COLORTERM = 'truecolor' },
      layout_config = { prompt_position = 'top' },
      sorting_strategy = 'ascending',
    },
  },
  fn = function(telescope)
    local builtin = require 'telescope.builtin';

    local function open_file_finder()
      if require 'git'.gitdir() then
        builtin.git_files()
      else
        builtin.find_files()
      end
    end

    keys.multimap {
      {'', '<C-p>', open_file_finder, {}},
      {'', '<C-p><C-p>', open_file_finder, {}},
      {'', '<C-p><C-f>', builtin.find_files, {}},
      {'', '<C-p><C-b>', builtin.buffers, {}},
      {'', '<C-p><C-g>', builtin.live_grep, {}},
      {'', '<C-p><C-q>', builtin.quickfix, {}},
      {'', '<C-p><C-r>', '<Cmd>OverseerRun<CR>', {}},
    }
  end
}

pkg {'dressing', setup = {}}
pkg {'overseer', setup = {}}

pkg {
  'neogen',
  setup = { snippet_engine = 'nvim' },
  fn = function(neogen) keys.map { '', '<leader>d', neogen.generate, default_opts }; end
}

pkg {'indent-blankline.nvim',
  require='ibl',
  setup = {
    indent = {char = "┊" },
    scope = {enabled = true, show_start = false, show_end = false, char = '|'},
  },
}

pkg {'gitsigns.nvim',
  require = 'gitsigns',
  setup = {
    signs = {
      add          = {text = ''},
      change       = {text = ''},
      delete       = {text = ''},
      topdelete    = {text = ''},
      changedelete = {text = ''},
      untracked    = {text = ''},
    },
    numhl = true,
  }
}

pkg 'nvim-treesitter'

vim.opt.foldmethod = 'expr'
vim.opt.foldexpr = 'nvim_treesitter#foldexpr()'
vim.opt.foldenable = true
vim.opt.foldlevelstart = 99
-- Borrowed in significant part from this reddit thread:
-- https://www.reddit.com/r/neovim/comments/psl8rq/sexy_folds/
vim.opt.fillchars = 'fold: '
vim.opt.foldminlines = 3
vim.opt.foldtext = table.concat(
  {
    [[substitute(getline(v:foldstart),'\\t',repeat('\ ',&tabstop),'g')]],
    "' ('", "(v:foldend - v:foldstart + 1)", "' lines) '",
    "trim(getline(v:foldend))",
  },
  '.'
)

vim.cmd "TSUpdate"

local lsp_servers = {
  pylsp = {},
  rust_analyzer = {},
  clangd = {},
  fsautocomplete = {},
  clojure_lsp = {},
  ocamllsp = {},
  ts_ls = { cmd = { 'npx', 'typescript-language-server', '--stdio' } },
  gopls = {},
  jdtls = {},
  wgsl_analyzer = {},
  solargraph = { cmd = { 'bundle', 'exec', 'solargraph', 'stdio' }},
}

pkg {'lspconfig',
    fn = function(lspconfig)
      for server_name, config in pairs(lsp_servers) do
      for k, v in pairs(require 'lsp_defaults') do
        config[k] = config[k] or v;
      end
      lspconfig[server_name].setup(config)
    end
  end
}

pkg {'colorizer', setup = {'css', 'scss', 'sass', 'vim', 'qml'}}

vim.opt.statusline=[[%{%luaeval("require 'statusline'()")%}]]

