local join = function(tbl) return table.concat(tbl, ',') end

return {join = join};
