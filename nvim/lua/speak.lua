local uv = vim.uv;
local handle = nil;
local position = nil;
vim.g.speakprg = vim.g.speakprg or 'espeak-ng'
vim.g.speakargs = vim.g.speakargs or {}

local cancel = function()
  if handle ~= nil then
    uv.process_kill(handle, nil);
    uv.close(handle);
    handle = nil
    return true
  end
  return false
end

local speak = function(lines)
  cancel();
  local stdin = uv.new_pipe();
  local stdout = uv.new_pipe();
  local stderr = uv.new_pipe();
  handle = uv.spawn(
    vim.g.speakprg,
    {
      stdio = {stdin, stdout, stderr},
      args = vim.g.speakargs,
    },
    function() handle = nil end
  );
  for _, l in ipairs(lines) do
    uv.write(stdin, l)
  end
  uv.shutdown(stdin);
end


local opfunc = function ()
  local text = vim.fn.getregion(vim.fn.getpos("'["), vim.fn.getpos("']"))
  speak(text);
  vim.fn.winrestview(position);
end

local line = function()
  local line = vim.api.nvim_win_get_cursor(0)[1];
  local text = vim.api.nvim_buf_get_lines(0, line - 1, line, nil);
  speak(text);
end

local visual = function()
  local text = vim.fn.getregion(vim.fn.getpos("."), vim.fn.getpos("v"), {type=vim.fn.mode()})
  speak(text);
end

local operator = function()
  position = vim.fn.winsaveview();
  vim.o.operatorfunc = "v:lua.require'speak'.opfunc"
  return "g@";
end

return {speak=speak, cancel=cancel, operator=operator, opfunc=opfunc, line=line, visual=visual};
