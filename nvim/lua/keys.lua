local table = require('table')
local function map(mapping)
  if (mapping[1] == '') then
    mapping[1] = 'nvo'
  end

  -- inner loop allows setting same keybindings for multiple modes at once
  -- e. g. mapkeys {'nv', '<C-j>', '<cmd>ThingToDoInNormalAndVisualMode'}
  for c in mapping[1]:gmatch('.') do
    local map = {unpack(mapping)}
    map[1] = c
    vim.keymap.set(unpack(map))
  end
end

local function multimap(keys)
  for _, mapping in ipairs(keys) do
    map(mapping)
  end
end

local function brace(mapping)
  local chaser, next, prev, opts = unpack(mapping)
  vim.keymap.set('', ']' .. chaser, next, opts)
  vim.keymap.set('', '[' .. chaser, prev, opts)
end

local function multibrace(keys)
  for _, mapping in ipairs(keys) do
    brace(mapping)
  end
end

return {map = map, multimap = multimap, brace = brace, multibrace = multibrace }
