
-- update diagnostics only when not in insert mode
-- found: https://www.reddit.com/r/neovim/comments/pfk209/nvimlsp_too_fast/
vim.lsp.handlers["textDocument/publishDiagnostics"] =
  vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, { update_in_insert = false })

local builtin = {
  K = 'hover',
  ['<F2>'] = 'rename',

  -- these map to the same key in terminals. Both are listed for gui compatibility
  ['<CR>'] = 'code_action',
  ['<C-CR>'] = 'code_action',
  ['<C-M>'] = 'code_action',
};

local telescope = {
  gd = 'definitions',
  gi = 'implementations',
  gr = 'references',
};

local function on_attach()
  vim.opt.formatexpr = 'v:lua.vim.lsp.formatexpr()';
  vim.opt.omnifunc = 'v:lua.vim.lsp.buf.omnifunc';
  for keymap, command in pairs(builtin) do
    vim.api.nvim_buf_set_keymap(0, '', keymap, '<cmd>lua vim.lsp.buf.' .. command .. '()<CR>', {noremap = true});
  end
  for keymap, command in pairs(telescope) do
    vim.api.nvim_buf_set_keymap(0, '', keymap, '<cmd>Telescope lsp_' .. command .. '<CR>', {noremap = true});
  end
end

return {on_attach = on_attach, capabilities = require('cmp_nvim_lsp').default_capabilities()}
