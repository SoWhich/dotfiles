local function call_setup(setup) return function (pkg) pkg.setup(setup) end end
local function not_lua() end

local function after_added(args)
  local setup_fns = {}

  if args.setup then
    table.insert(setup_fns, call_setup(args.setup))
  end

  if args.fn then
    table.insert(setup_fns, args.fn)
  end

  if (#setup_fns == 0) then
    return not_lua
  else
    return function (req_path)
      for _, fn in pairs(setup_fns) do fn(require(req_path)) end
    end
  end
end

local function pkg(pkg)
  local do_after = after_added(pkg)
  local require_path = pkg.require or pkg[1];
  local packpath = pkg[1] or pkg;

  vim.cmd(':packadd '..packpath)
  do_after(require_path)
end

return pkg
