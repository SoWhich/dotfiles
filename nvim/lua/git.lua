
local Job = require 'plenary.job'
local function gitdir()
  local status = Job:new{'git', 'status' }
  status:start()
  local _, code = status:sync()
  return code == 0
end
return {gitdir = gitdir}
