local columns = {
  c = 80,
  cpp = 80,
  python = 89,
  rust = 101,
}

return function()
  vim.opt_local.colorcolumn = tostring(columns[vim.opt.filetype:get()] or 80)
end
