-- Utility functions
local nexthourglass = {
  ['⠏'] = '⠛',
  ['⠛'] = '⠹',
  ['⠹'] = '⠼',
  ['⠼'] = '⠶',
  ['⠶'] = '⠧',
  ['⠧'] = '⠏',
}

local function hourglass()
  local glass = {display = '⠏', time = os.time()}
  return function()
    local time = os.time()
    if time > glass.time then
      glass.display = nexthourglass[glass.display];
      glass.time = time;
    end
    return glass.display
  end
end

local function eval(parts, ptable)
  if type(parts) == 'function' then
    eval(parts(), ptable)
  elseif type(parts) == 'table' then
    for i, component in ipairs(parts) do
      eval(component, ptable)
    end
  elseif parts == '' then
    return
  else
    table.insert(ptable, parts)
  end
end


local function highlight(group, parts)
  if tostring(vim.fn.win_getid()) ~= tostring(vim.g.actual_curwin) then
    group = 'StatusLineNC'
  end
  return {"%#".. group .. "#", parts, "%0*"}
end

-- Statusline Entry functions

local function git_delta()
  local status = vim.b.gitsigns_status
  if status and status ~= '' then
   return highlight('StatusVCS', {' ', status, ' '})
  else
    return ''
  end
end


local function git_head()
  local git_head = vim.b.gitsigns_head
  if git_head and git_head ~= '' then
   return highlight('StatusVCS', {' ', git_head, ' ', '', ' '})
  else
    return ''
  end
end

local function diagnostic(sev, icon)
  return function()
    local errors = #vim.diagnostic.get(0, {severity = vim.diagnostic.severity[sev]})
    if errors ~= 0 then
      return highlight('Status' .. sev, {' ', icon, errors, ' '})
    else
      return ''
    end
  end
end

local function treesitter_position()
  local pos = require('nvim-treesitter').statusline()
  if pos ~= nil then
     return pos
  else
     return ''
  end
end

local function filepath_special()
  local filename = vim.fn.bufname()
  local parts = vim.split(filename, '/', {plain = true});
  local _justfile = table.remove(parts);
  local singles = vim.tbl_map(function(seg) return string.sub(seg, 1, 1) end, parts)
  local joined = table.concat(singles, '/')
  if joined == '' then
    return ''
  else
    return joined .. '/'
  end
end

local function filename_special()
  local filename = vim.fn.bufname()
  local parts = vim.split(filename, '/', {plain = true});
  return table.remove(parts);
end

-- final statusline
return function()
  local parts = {}
  eval({
    highlight('StatusPath', {' ', filepath_special}),
    highlight('StatusFile', {filename_special, '%M '}),
    ' ',

    git_delta,

    ' ',
    highlight('StatusLineNC', {'%<', treesitter_position, '%='}),
    ' ',

    diagnostic('WARN', ' '),
    diagnostic('ERROR', ' '),

    ' ',
    highlight( 'StatusLoc', {' %l:%v '}),

    ' ',
    git_head,

    ' ',
    highlight('StatusFile', {' %Y%r '}),
  }, parts);
  return table.concat(parts, '');
end
