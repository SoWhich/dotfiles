local util = require 'util'

return function ()
  vim.opt_local.listchars = util.join { 'tab: ⣀', 'trail:░', 'eol:⏎' }
  vim.opt_local.linebreak = true;
end
