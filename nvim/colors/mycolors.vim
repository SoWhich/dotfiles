"runtime colors/slate.vim

"""""""""""""" UI colors """"""""""""""
" hi Normal guifg=White
hi ColorColumn guibg=#181023
" hi Conceal
" hi Cursor
" hi lCursor
" hi CursorIM
" hi CursorColumn
" hi CursorLine guibg=#FFFFFF
" hi Directory
" hi DiffAdd
" hi DiffChange
" hi DiffDelete
" hi DiffText
hi EndOfBuffer guifg=#332255
" hi TermCursor
" hi TermCursorNC
" hi ErrorMsg
" hi VertSplit
hi Folded gui=bold guibg=#222222 guifg=#77AA88
" hi FoldColumn
" hi SignColumn
" hi IncSearch
" hi Substitute
hi LineNr guifg=#80AEEE guibg=#0C0C0C
" hi CursorLineNr
hi MatchParen gui=bold guifg=#EEAE80 guibg=#000000
" hi ModeMsg
" hi MsgArea
" hi MsgSeparator
" hi MoreMsg
" hi NonText
hi NormalFloat blend=20 guibg=#222222
" hi NormalNC
hi Pmenu guibg=#444444 guifg=White
hi PmenuSel blend=0 guibg=#444474 guifg=White
" hi PmenuSbar
" hi PmenuThumb
hi Question guibg=#222222 guifg=#DDCCFF
" hi QuickFixLine
hi Search gui=bold guibg=#222222 guifg=NONE
" hi SpecialKey
" hi SpellBad
" hi SpellCap
" hi SpellLocal
" hi SpellRare
" hi TabLine
" hi TabLineFill
" hi TabLineSel
hi Title gui=bold guifg=#FFFFAA
hi Visual guifg=#000000 guibg=#FFFFFF
" hi VisualNOS
" hi WarningMsg
hi Whitespace guifg=#777777 gui=None
" hi WildMenu

"""""""""""""" syntax colors """"""""""""""
hi Comment gui=italic guifg=#9977BB

hi Constant guibg=Black guifg=#99CCFF
hi String guifg=#CC99FF
" hi Character guifg=#EEEECC
" hi Number guifg=#CCCCFF
" hi Float guifg=#CCCCFF
" hi Boolean guifg=#FFCCCC

hi Identifier guifg=White
hi Function gui=italic guifg=#CCBBEE

hi Statement gui=NONE guifg=#77CCCC
" #AA77FF
" hi Conditional gui=NONE guifg=
" hi Repeat gui=bold guifg=#FFCCAA
" hi Label gui=NONE guifg=#AAAAAA
hi Operator gui=NONE guifg=#99CCFF
" hi Keyword  gui=bold guifg=#FFFFAA
" hi Exception gui=bold guifg=#CC7777

hi PreProc guifg=#9955AA
" hi Include
" hi Define
" hi Macro
" hi PreCondit

hi Type gui=NONE guifg=#9999FF
" hi StorageClass
" hi Structure
" hi Typedef
"
hi Special gui=NONE guifg=#CCaaEE
" hi SpecialChar
" hi Tag
" hi Delimiter
" hi SpecialComment
" hi Debug
"
" hi Underlined
"
" hi Ignore
"
" hi Error
"
" hi Todo

"""""""""""""" gitsigns colors """"""""""""""
hi GitSignsAddNr guifg=#80EEAE guibg=#0C0C0C
hi GitSignsChangeNr guifg=#EEAE80 guibg=#0C0C0C
hi GitSignsDeleteNr guifg=#EE80AE guibg=#0C0C0C

"""""""""""""" treesitter colors """"""""""""""
hi TSVariableBuiltin guifg=#FFCCCC
hi TSSymbol guifg=#DDFFDD

"""""""""""""" custom statusline colors """"""""""""""
hi StatusLine gui=bold guibg=#DDCCFF guifg=Black
hi StatusLineNC gui=NONE guibg=#DDCCFF guifg=Black
hi StatusPath guibg=#331155 guifg=#AAAAAA gui=NONE
hi StatusFile guibg=#331155 guifg=White gui=NONE
hi StatusFiller gui=bold guibg=#DDCCFF guifg=Black
hi StatusLoc guibg=#335511 guifg=White gui=NONE
hi StatusVCS guibg=#AA88CC guifg=#552211 gui=bold
hi StatusWARN guibg=#180931 guifg=Yellow
hi StatusERROR guibg=#180931 guifg=Red
